<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Login - Application</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link href="//netdna.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <!--Pulling Awesome Font -->
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
    <style>
    @import url(http://fonts.googleapis.com/css?family=Roboto:400);
    body {
    background-color:#fff;
    -webkit-font-smoothing: antialiased;
    font: normal 14px Roboto,arial,sans-serif;
    }

    .container {
        padding: 25px;
        position: fixed;
    }

    .form-login {
        background-color: #EDEDED;
        padding-top: 10px;
        padding-bottom: 20px;
        padding-left: 20px;
        padding-right: 20px;
        border-radius: 15px;
        border-color:#d2d2d2;
        border-width: 5px;
        box-shadow:0 1px 0 #cfcfcf;
    }

    h4 { 
    border:0 solid #fff; 
    border-bottom-width:1px;
    padding-bottom:10px;
    text-align: center;
    }

    .form-control {
        border-radius: 10px;
    }

    .wrapper {
        text-align: center;
    }
    </style>
</head>
<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-3">
            <form method="POST" action="{{ route('login') }}">
            @csrf
            <div class="form-login">
                <h3>Welcome</h3>
                <input id="email" type="email" class="form-control input-sm chat-input @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                </br>
                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br><br>
                @enderror
                <input id="password" type="password" class="form-control input-sm chat-input @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                </br>
                @error('password')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                    <br><br>
                @enderror
                <div class="wrapper">
                    <span class="group-btn">
                        <button type="submit" class="btn btn-primary">{{ __('Login') }} <i class="fa fa-sign-in"></i></button>    
                    </span>
                </div>
            </div>
            </form>        
        </div>
    </div>
</div>
</body>
</html>