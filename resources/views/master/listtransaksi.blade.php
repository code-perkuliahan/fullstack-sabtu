@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6 col-lg-12">
            <div class="card">
                <div class="card-header"><a href="{{ url('transaksi/create') }}" class="btn btn-primary">Input Data</a></div>
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div id="pie_chart" style="width:500px; height:450px;"></div>
                        </div>
                        <div class="col-md-6">
                            <div id="pie_lokasi" style="width:500px; height:450px;"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('custom-page-script')
<script src="http://code.highcharts.com/highcharts.js"></script>
<script src="http://code.highcharts.com/modules/exporting.js"></script>
<script type="text/javascript">
    $(document).ready(function() {
        // UNTUK AMBIL DATA BARANG
        var barang = <?php echo json_encode($getBarang); ?>;
        var options = {
            chart: {
                renderTo: 'pie_chart',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },

            title: {
                text: 'Persentase Data Pengiriman - Barang'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>',
                percentageDecimals: 2
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(0) + ' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Total Persentase'
            }]
        }

        // KOMPONEN UTAMA - UNTUK LIST DATA DARI CONTROLLER
        myarray = [];
        $.each(barang, function(index, val) {
            myarray[index] = [val.nama_barang, val.total];
        });

        options.series[0].data = myarray;
        chart = new Highcharts.Chart(options);

        //GET DATA LOKASI
        var lokasi = <?php echo json_encode($getLokasi); ?>;
        var options_lokasi = {
            chart: {
                renderTo: 'pie_lokasi',
                plotBackgroundColor: null,
                plotBorderWidth: null,
                plotShadow: false
            },

            title: {
                text: 'Persentase Data Pengiriman - Lokasi'
            },
            tooltip: {
                pointFormat: '{series.name}: <b>{point.percentage:.0f}%</b>',
                percentageDecimals: 2
            },

            plotOptions: {
                pie: {
                    allowPointSelect: true,
                    cursor: 'pointer',
                    dataLabels: {
                        enabled: true,
                        color: '#000000',
                        connectorColor: '#000000',
                        formatter: function() {
                            return '<b>' + this.point.name + '</b>: ' + this.percentage.toFixed(0) + ' %';
                        }
                    }
                }
            },
            series: [{
                type: 'pie',
                name: 'Total Persentase'
            }]
        }

        // KOMPONEN UTAMA - UNTUK LIST DATA DARI CONTROLLER
        array_lokasi = [];
        $.each(lokasi, function(index, val) {
            array_lokasi[index] = [val.nama_lokasi, val.total];
        });

        options_lokasi.series[0].data = array_lokasi;
        chart = new Highcharts.Chart(options_lokasi);
    });
</script>
@endsection