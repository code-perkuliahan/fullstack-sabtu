@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <form method="post" action="{{ url('transaksi') }}">
                <div class="card">
                    <div class="card-header"><h3>Transaksi Pengiriman</h3></div>
                    <div class="card-body">
                        <div class="form-group{{ $errors->has('no_transaksi') ? ' alert alert-danger' : '' }}">
                            <label class ='control-label col-md-3 col-sm-3 col-xs-12'>No. Transaksi</label>
                            <div class="item col-sm-9">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <input type="text" name="no_transaksi" autocapitalize="characters" class="form-control"
                                    value="{{ old('no_transaksi') }}">
                                </div>
                            </div>
                            <!-- errors -->
                            @if($errors->has('no_transaksi')) 
                            <span class="help-block"><strong>{{ $errors->first('no_transaksi') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('barang_id') ? ' alert alert-danger' : '' }}">
                            <label class ='control-label col-md-3 col-sm-3 col-xs-12'>Kode Barang</label>
                            <div class="item col-sm-9">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="barang_id" class="form-control">
                                        <option value="">Pilih barang</option>
                                        @foreach($listbrg as $items)
                                        <option value="{{ $items->id }}" {{ old("barang_id") == $items->id ? "selected" : "" }}>{{ $items->nama_barang }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('barang_id')) 
                                <span class="help-block"><strong>{{ $errors->first('barang_id') }}</strong></span>
                            @endif
                        </div>
                        <div class="form-group{{ $errors->has('lokasi_id') ? ' alert alert-danger' : '' }}">
                            <label class ='control-label col-md-3 col-sm-3 col-xs-12'>Kode Lokasi</label>
                            <div class="item col-sm-9">
                                <div class="col-md-6 col-sm-6 col-xs-12">
                                    <select name="lokasi_id" class="form-control">
                                        <option value="">Pilih lokasi</option>
                                        @foreach($listlok as $items)
                                        <option value="{{ $items->id }}" {{ old("lokasi_id") == $items->id ? "selected" : "" }}>{{ $items->nama_lokasi }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            @if($errors->has('lokasi_id')) 
                                <span class="help-block"><strong>{{ $errors->first('lokasi_id') }}</strong></span>
                            @endif
                        </div>
                    </div>
                    <div class="card-footer">
                        <a href="{{ url('transaksi') }}" class="btn btn-danger">Cancel</a>
                        <button type='submit' id = 'btnSubmit' class='btn btn-success btn-xlg bigger-100 radius-4'>Submit</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection