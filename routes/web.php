<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'auth'], function(){
    Route::get('/', function () {
        return redirect('/login');
    });    
});

Route::resource('barang','Master\BarangController');
Auth::routes();

//Route::group(['middleware' => 'auth'], function(){
    Route::get('/home', 'HomeController@index')->name('home');
    #Route::get('/transaksi','TransaksiController@index');

    Route::get('/transaksi','TransaksiController@getBarangTotal')->name('transaksi.index');
    Route::get('/transaksi/create','TransaksiController@create');
    Route::post('/transaksi','TransaksiController@simpan');
//});
