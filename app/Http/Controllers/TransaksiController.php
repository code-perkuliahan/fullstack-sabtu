<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class TransaksiController extends Controller
{
    public function index()
    {
        $list = DB::table('pengiriman')->get();

        return response()->json(['data' => $list]);
    }

    public function create()
    {
        $listbrg = DB::table('barang')->select('id','nama_barang')->get();
        $listlok = DB::table('lokasi')->select('id','nama_lokasi')->get();
        
        return view('master.formtransaksi', compact('listbrg','listlok'));
    }

    public function simpan(Request $request )
    {
        #setting
        $messages = [
            'no_transaksi.required' => 'No Transaksi wajib diisi oleh user',
            'no_transaksi.unique' => 'No Transaksi tidak boleh sama dengan yang sebelumnya',
            'barang_id.required' => 'Data barang wajib dipilih oleh user',
            'lokasi_id.required' => 'Data lokasi wajib dipilih oleh user',
            ];

        $input = $request->all();
        $validator = Validator::make($input, [
            'no_transaksi' => 'required|unique:pengiriman,no_pengiriman',
            'barang_id' => 'required', 'lokasi_id' => 'required'
        ], $messages);

        #RETURN VALIDATOR
        if($validator->fails())
        {
            $messages = $validator->messages();
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }

        #CHECK GUDANG
        $ceksisa = DB::table('pengiriman')->where('lokasi_id',$request->input('lokasi_id'))
            ->count();
        if($ceksisa > 5){
            $messages = array('lokasi_id' => 'Ketersediaan kapasitas gudang tidak cukup');
            return Redirect::back()->withErrors($messages)->withInput($request->all());
        }

        DB::table('pengiriman')->insertGetId(['no_pengiriman'=> $request->input('no_transaksi'),
            'barang_id' => $request->input('barang_id'), 'lokasi_id' => $request->input('lokasi_id'),
            'tanggal' => Carbon::now(), 'jumlah_barang' => 1, 'harga_barang' => 10000]);
        
        return redirect('transaksi')->with('success','Input data transaksi berhasil');
    }

    public function getBarangTotal()
    {
        $getBarang = DB::table('pengiriman')->join('barang','barang.id','pengiriman.barang_id')->
            select('nama_barang', DB::raw("count(pengiriman.id) as total"))->
            groupBy('barang_id')->get();

        $getLokasi = DB::table('pengiriman')->join('lokasi','lokasi.id','pengiriman.lokasi_id')->
            select('nama_lokasi', DB::raw("count(pengiriman.id) as total"))->
            groupBy('lokasi_id')->get();

        return view('master.listtransaksi',compact('getBarang','getLokasi'));
    }
}
